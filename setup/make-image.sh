#!/bin/bash

set -e

if [ $# != "2" ] || [ ! -b "$2" ]; then
  echo "Usage: make-image-linux.sh <hosted Inferno directory> <device file>";
  exit 1;
fi

INFERNO_ROOT="$1"
DEVICE="$2"

echo "Making a boot image in $DEVICE"
dd if=/dev/zero of="$DEVICE" bs=1M count=100

echo "Creating a FAT partition"
#fdisk /dev/sdc
# n p <Return> +100MB t 1 4 w

#parted -s /dev/sdc mklabel msdos mkpart primary fat16 0 100MB

#escaped=`echo $DEVICE | sed 's/\//\\\\\//g'`
#sed "s/\/dev\/sdX/$escaped/g" sfdisk-template.txt > partitions.txt

sfdisk "$DEVICE" < setup/sfdisk-template.txt

echo "Formatting the FAT partition"
PARTITION=${DEVICE}1
mkfs.fat "$PARTITION"

echo "Mounting the filing system"
mount "$PARTITION" /media/disk

echo "Copying boot files"
for name in bootcode.bin config.txt fixup_cd.dat LICENCE.broadcom start_cd.elf;
  do cp boot/$name /media/disk/;
done

cp $INFERNO_ROOT/os/rpi/kernel.bin /media/disk/

echo "Creating and copying system files"
mkdir /media/disk/tmp

for name in dis doc fonts icons keydb lib locale man module services usr; do
  echo $name
  cp -r $INFERNO_ROOT/$name /media/disk/
done

echo "Unmounting the filing system"
umount /media/disk

echo "Ejecting the device"
eject "$DEVICE"

echo "Now unplug the SD card"
