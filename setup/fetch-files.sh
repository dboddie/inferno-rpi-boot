#!/bin/bash

set -e

if [ ! -d boot ]; then
  mkdir boot
fi
 
wget -P boot https://github.com/raspberrypi/firmware/raw/master/boot/LICENCE.broadcom
wget -P boot https://github.com/raspberrypi/firmware/raw/master/boot/bootcode.bin
wget -P boot https://github.com/raspberrypi/firmware/raw/master/boot/fixup_cd.dat
wget -P boot https://github.com/raspberrypi/firmware/raw/master/boot/start_cd.elf
