# Setup Scripts for Inferno on the Raspberry Pi

This repository contains a collection of scripts and files for helping to set
up the Inferno operating system on a Raspberry Pi.

## Requirements

The scripts are intended to be run on a separate GNU/Linux system, using
standard tools such as `wget`, `dd`, `sfdisk` and `mkfs.fat`.

You must also set up a hosted build of Inferno and build the kernel for the
Raspberry Pi. The sources for Inferno on the Raspberry Pi can be obtained from
[the inferno-rpi repository](https://bitbucket.org/infpi/inferno-rpi/).

## Building a Kernel

Clone the inferno-rpi repository and enter the working directory.

Edit the `mkconfig` file to configure the build to your desktop system, not the
Raspberry Pi. For example, for a 64-bit Intel/AMD system running Linux with the
inferno-rpi working directory in `/tmp/inferno-rpi`, modify the following
values in the `mkconfig` file:

    ROOT=/tmp/inferno-rpi
    SYSHOST=Linux
    OBJTYPE=386

Run the initial build:

    ./makemk.sh


If this succeeds then build and install the hosted system in the repository's
working directory:

```
export PATH=$PWD/Linux/386/bin
mk install
```

You can test this by running the `emu` program to enter the hosted environment.
Type `exit` to leave the hosted environment.

Now enter the directory containing the Raspberry Pi port and build the kernel,
removing some unused commands from the `mkfile` first:

    cd os/rpi
    sed -ir 's/cp irpi/#cp irpi/g' mkfile
    mk

The result should be `kernel.bin` file in the current directory.

### Useful Changes

One useful modification is to enable the UART console. This can be done by
uncommenting the following line in the `main.c` file:


    //uartconsinit();


Then rebuild the kernel with the change in the same way as before.

## Making a Boot Image

Enter the working directory of this repository and run the script to fetch the
firmware files for the Raspberry Pi from the [remote repository](https://github.com/raspberrypi/firmware):

    setup/fetch-files.sh

If these were downloaded successfully, attach a blank micro-SD card to your
system and run the script to make a boot image, passing the working directory
of the Inferno repository and the device file for the micro-SD card.

**Make certain that the device file you give to the script corresponds to the
micro-SD you want to use. Any existing data on the device you use will be
destroyed.**

For example, if the Inferno repository was cloned to `/tmp/inferno-rpi` and the
micro-SD card is present at `/dev/sdc` then the following command should make
and install a boot image on the card:

    sudo setup/make-image.sh /tmp/inferno-rpi /dev/sdc

If this succeeds then you can install the card in the Raspberry Pi and power it
up. If you have connected it to an HDMI monitor then you should see a terminal.

If you changed the kernel to enable the UART console, attach a console cable as
[described here](https://learn.adafruit.com/raspberry-pi-zero-creation/give-it-life)
and start a terminal program such as `picocom` like this:

    picocom -b 115200 /dev/ttyUSB0

This assumes that `/dev/ttyUSB0` is the device used for the console.

When the Raspberry Pi is powered up, you should see something like this on the
console output:

    00009D5C
    Entered main() at 00009D50 with SP=00002FCC with SC=0005307D with CPSR=600001D3 with SPSR=00000010
    Clearing Mach:  00002000-000020CC
    Clearing edata: 001B91B0-001C9E10
    #l0: usb: 100Mbps port 0x0 irq -1: 000000000000
    #l1: usb: 100Mbps port 0x0 irq -1: 000000000000
    #l2: usb: 100Mbps port 0x0 irq -1: 000000000000
    #l3: usb: 100Mbps port 0x0 irq -1: 000000000000
    i2csetup
    ;

This should give you an environment to experiment with.

## Troubleshooting

If the boot procedure fails, ensure that the board is powered appropriately,
using a dedicated power supply if possible.

Check to see if the micro-SD card contains a single FAT16 partition and that it
contains at least the following files:

* `bootcode.bin`
* `config.txt`
* `fixup_cd.dat`
* `start_cd.elf`

If the FAT16 partition is not recognised by the kernel then check the partition
type using a program such as `fdisk`. It should have a type (or ID) of 4.

License
-------

Written in 2019 by David Boddie <david@boddie.org.uk>

To the extent possible under law, the author(s) have dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <https://creativecommons.org/publicdomain/zero/1.0/>.
